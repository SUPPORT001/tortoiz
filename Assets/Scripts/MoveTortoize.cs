using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveTortoize : MonoBehaviour
{
    public Image tortoiz;
    public Canvas monitor;
    internal float speed = 3;
    internal int moveStart = 2;
    internal string rotationTrue;
    internal float maxXX, minXX, maxYY, minYY;
    // Start is called before the first frame update
    void Start()
    {
        maxXX = 145;
        minXX = -146;
        maxYY = 69;
        minYY = -71;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move(string rotation)
    {
        rotationTrue = rotation;
        
            

            switch (rotation)
            {
                case "up":
                    MoveUp();
                    break;
                case "left":
                    MoveLeft();
                    break;
                case "right":
                    MoveRight();
                    break;
                case "down":
                    MoveDown();
                    break;
            }
        
    }

    void MoveUp()
    {
        if (tortoiz.transform.position.y < maxYY)
        {
            tortoiz.transform.position += new Vector3(0, speed, 0);
        }
    }
    public void MoveLeft()
    {
        if (tortoiz.transform.position.x > minXX)
        {
            tortoiz.transform.position += new Vector3(-speed, 0, 0);
        }
    }
    public void MoveRight()
    {
        if (tortoiz.transform.position.x < maxXX)
        {
            tortoiz.transform.position += new Vector3(speed, 0, 0);
        }
    }
    public void MoveDown()
    {
        if( tortoiz.transform.position.y > minYY)
        {
            tortoiz.transform.position += new Vector3(0, -speed, 0);
        }
    }
}
